
# DummyProject

Some description



## Usage

These instructions will get you a copy of the project, running on your local machine.


#### Get the source code

Download the zipped [source code](https://gitlab.com/scheckmate/VSIDummyRepo/repository/master/archive.zip) and extract it somewhere
on your local filesystem

**Alternatively** you can also clone the repository to your local filesystem directly via git.

```
git clone https://gitlab.com/scheckmate/VSIDummyRepo.git
```




#### Use under Windows

The project was developed and tested with Python3.6

For the use under Windows (64 bit) we recommend [Anaconda Python3 x64](https://repo.continuum.io/archive/.winzip/Anaconda3-5.1.0-Windows-x86_64.zip)
which is a free development environment for the Python programming language. Anaconda has a package management system ```conda``` which allows
the usage of multiple virtual environments.

Nevertheless, feel free to use any other desired environment for the development and/or execution of the ```dynasaur``` project.



###### Setup dummy with Anaconda

Download the Anaconda [environment file](dynasaur_env.txt), which contains information about the required python packages.

Open the installed ```Anconda Prompt``` and type the following to import the environment

```
conda create --file dynasaur_env.txt -n dynasaur
```

Activate it afterwards:

```
activate dynasaur
```

Change the directory to the location of the source code.

```
cd path\to\source\code
```

Run dynasaur:

```
python dynasaur.py
```